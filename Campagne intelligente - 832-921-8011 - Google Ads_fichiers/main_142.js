self.$__dart_deferred_initializers__=self.$__dart_deferred_initializers__||Object.create(null)
$__dart_deferred_initializers__.current=function(a,b,c,$){var C,H,J,P,W,G,M,B,S,Q,K={
Eg:function(d,e,f,g,h){var x=new K.amU(d,e,f,h,g)
x.f="Tag { startIdx: "+d+", endIdx: "+e+", tag: "+f+", value: "+H.p(h)+", isIncompleteTag: "+g+" }"
return x},
fJL:function(d,e,f){var x,w,v,u=H.a([],y.f)
for(x=d.length,w=0;w<x;){v=K.fpd(d,w,e,!1)
if(v!=null){u.push(v)
w=v.b}else ++w}return u},
fpd:function(a1,a2,a3,a4){var x,w,v,u,t,s,r,q,p,o,n,m,l,k,j,i,h,g,f,e,d=null,a0="ifTag"
if(a1[a2]!=="{")return
x=C.b.ey(a1,"}",a2)
w=C.b.ey(a1,":",a2)
v=a2+1
u=C.b.ey(a1,"{",v)
t=C.b.ey(a1,"=",a2)
if(!a3&&t===v){s=x===-1
r=C.b.bf(a1,t,s?a1.length:x)
if(C.b.cr(r.toLowerCase(),"=if")){r=C.b.bS(C.b.cm(r,3))
if(C.b.cr(r,"(")){q=K.dDY(r,1,",")
v=r.length
if(q===v)return K.Eg(a2,s?a1.length:x+1,a0,!0,d)
p=q+1
o=K.dDY(r,p,")")
n=C.b.bS(C.b.bf(r,p,o))
if(o===v)return K.Eg(a2,s?a1.length:x+1,a0,!0,n)
if(o===C.b.bS(r).length-1)return K.Eg(a2,s?a1.length:x+1,a0,!0,n)
m=C.b.ey(r,":",o)
if(m===-1)return K.Eg(a2,s?a1.length:x+1,a0,!0,d)
if(C.b.bS(C.b.bf(r,o+1,m))!=="")return K.Eg(a2,s?a1.length:x+1,a0,!0,d)
l=C.b.bS(C.b.cm(r,m+1))
v=s?a1.length:x+1
return K.Eg(a2,v,a0,s,n.length>l.length?n:l)}}k=P.cd("\\=([^{]*)\\.(.*)\\:(.*)",!1,!1)
if(k.b.test(r)&&C.b.cr(r,k.fO(r).b[0]))if(C.b.ar(r,":")){l=C.b.bS(C.b.cm(r,C.b.cI(r,":")+1))
return K.Eg(a2,s?a1.length:x+1,"dataTag",!1,l)}if(u===-1||u>x)return K.Eg(a2,s?a1.length:x+1,"",!1,d)}s=x===-1
if(s&&w===-1)return K.cX_(a1,a2,d,a3,!1)
if(s)j=w
else j=w===-1?x:Math.min(x,w)
i=C.b.bf(a1,v,j)
if(!J.J3(C.zx.a,i.toLowerCase())){v=a1[j]
if(v!=="}"&&v!==":")return K.cX_(a1,a2,d,a3,!1)
if(u!==-1&&u<j)return K.cX_(a1,a2,u,a3,!1)
return}if(a1[j]==="}"){v=K.Eg(a2,j+1,i,!1,d)
return v}for(h=j+1,v=a1.length,g="",f=1;h<v;++h){e=a1[h]
if(e==="{")++f
else if(e==="}")--f
if(f>0)g+=e
else{v=h+1
s=new K.amU(a2,v,i,g,!1)
s.f="Tag { startIdx: "+a2+", endIdx: "+v+", tag: "+i+", value: "+g+", isIncompleteTag: false }"
return s}}v=K.Eg(a2,v,i,!1,g)
return v},
cX_:function(d,e,f,g,h){var x,w,v,u,t,s,r=e+1,q=C.b.cm(d,r).toLowerCase(),p=f==null?q.length:f
for(x=J.aV(J.aCL(C.zx.a)),w=0;x.ag();){v=x.gak(x)
u=0
while(!0){if(!(u<p&&u<v.length&&q[u]===v[u]))break;++u}w=Math.max(w,u)}t=C.b.bf(q,0,w)
s=r+w
if(g){if(t.length!==0&&J.ni(C.zx.by(0,0),t))return K.Eg(e,s,t,!0,null)
return}return K.Eg(e,s,t,!0,null)},
dDY:function(d,e,f){var x,w,v,u
for(x=d.length,w=e,v=0;w<x;){u=d[w]
if(u===f&&v===0)break
if(u==="(")++v
if(u===")")--v;++w}return w},
amU:function amU(d,e,f,g,h){var _=this
_.a=d
_.b=e
_.c=f
_.d=g
_.e=h
_.f=null}},O,N,X,R,A,L,Y,Z={
cYR:function(d,e,f){var x,w,v,u,t,s,r
if(d==null)return 0
d=C.b.bS(d)
x=d.length
if(x===0)return 0
w=K.fJL(d,e,!1)
for(v=0,u=0,t=0;!0;++t){s=t<w.length?w[t].a:x
if(v<s)u+=Z.e3M(C.b.bf(d,v,s))
if(t===w.length)break
r=w[t]
u+=Z.e3M(r.d)
v=r.b}return u},
e3M:function(d){var x,w
if(d==null)return 0
for(x=new H.Zx(d),x=new H.hG(x,x.ga6(x),y.g.i("hG<aW.E>")),w=0;x.ag();)w+=Z.fpl(x.d)
return w},
fpl:function(d){var x
if(!(d<=1273))if(d!==1470)if(!(d>=1488&&d<=1514))if(d!==1523)if(d!==1524)if(!(d>=1536&&d<=1791))if(!(d>=1872&&d<=1919))if(!(d>=64336&&d<=65023))if(!(d>=65136&&d<=65279))if(!(d>=7680&&d<=8367))if(!(d>=8448&&d<=8506))if(!(d>=2304&&d<=3455))if(!(d>=3584&&d<=3711))x=d>=65377&&d<=65500
else x=!0
else x=!0
else x=!0
else x=!0
else x=!0
else x=!0
else x=!0
else x=!0
else x=!0
else x=!0
else x=!0
else x=!0
else x=!0
if(x)return 1
return 2}},V,U,T,F,E,D
a.setFunctionNamesIfNecessary([K,Z])
C=c[0]
H=c[1]
J=c[2]
P=c[3]
W=c[4]
G=c[5]
M=c[6]
B=c[7]
S=c[8]
Q=c[9]
K=a.updateHolder(c[10],K)
O=c[11]
N=c[12]
X=c[13]
R=c[14]
A=c[15]
L=c[16]
Y=c[17]
Z=a.updateHolder(c[18],Z)
V=c[19]
U=c[20]
T=c[21]
F=c[22]
E=c[23]
D=c[24]
K.amU.prototype={
am:function(d,e){var x=this
if(e==null)return!1
return e instanceof K.amU&&e.a===x.a&&e.b===x.b&&e.c===x.c&&e.d==x.d&&e.e===x.e},
gaB:function(d){var x=this
return X.k5([x.a,x.b,x.c,x.d,x.e])},
X:function(d){return this.f},
gaf:function(d){return this.d}}
var z=a.updateTypes([]);(function inheritance(){var x=a.inherit
x(K.amU,P.C)})()
H.ac(b.typeUniverse,JSON.parse('{}'))
var y={g:H.b("Zx"),f:H.b("f<amU>")};(function constants(){var x=a.makeConstList
C.bF_=H.a(x(["keyword","param1","param2"]),H.b("f<c>"))
C.bSE=new H.a4(3,{keyword:null,param1:null,param2:null},C.bF_,H.b("a4<c,R>"))
C.zx=new P.IH(C.bSE,H.b("IH<c>"))})()}
$__dart_deferred_initializers__["U0DCwoejeJaS7CmJekTtFPU4QDo="] = $__dart_deferred_initializers__.current
//# sourceMappingURL=main.dart.js_129.part.js.map
