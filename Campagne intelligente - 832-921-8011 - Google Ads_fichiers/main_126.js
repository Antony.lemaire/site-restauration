self.$__dart_deferred_initializers__=self.$__dart_deferred_initializers__||Object.create(null)
$__dart_deferred_initializers__.current=function(a,b,c,$){var C,H,J,P,W,G,M,B,S,Q,K,O,N,X,R,A,L,Y,Z,V,U={
duW:function(d,e){var x,w=new U.aRx(N.I(),N.I(),E.ad(d,e,3)),v=$.duY
if(v==null)v=$.duY=O.al($.h7d,null)
w.b=v
x=document.createElement("express-ad-preview-card")
w.c=x
return w},
aRx:function aRx(d,e,f){var _=this
_.e=d
_.f=e
_.c=_.b=_.a=_.go=_.fy=_.fx=_.fr=_.dy=_.dx=_.db=_.cy=_.cx=_.ch=_.Q=_.z=_.y=_.x=_.r=null
_.d=f}},T={
fnf:function(){return H.cg("dialog.1")},
apz:function apz(d,e){var _=this
_.d=_.c=_.b=_.a=null
_.e=!1
_.f=d
_.r=e
_.x=null},
aDh:function aDh(){},
aX8:function aX8(){}},F={bya:function bya(){}},E,D
a.setFunctionNamesIfNecessary([U,T,F])
C=c[0]
H=c[1]
J=c[2]
P=c[3]
W=c[4]
G=c[5]
M=c[6]
B=c[7]
S=c[8]
Q=c[9]
K=c[10]
O=c[11]
N=c[12]
X=c[13]
R=c[14]
A=c[15]
L=c[16]
Y=c[17]
Z=c[18]
V=c[19]
U=a.updateHolder(c[20],U)
T=a.updateHolder(c[21],T)
F=a.updateHolder(c[22],F)
E=c[23]
D=c[24]
T.apz.prototype={
aPf:function(){var x,w=this
w.r.aO(C.P,"FullPageAdPreviewClicked")
if(w.x!=null){w.Yk()
x=w.x
x.f=x.e=!0}else w.f.qc()},
pV:function(d,e){var x
this.x=y.z.a(e.c)
this.Yk()
x=this.x
x.f=x.e=!0},
Yk:function(){var x=this,w=x.x
w.a=x.a
w.b=x.b
w.c=x.d
w.d=x.e},
gee:function(){return this.c},
gdj:function(){return this.d},
ghd:function(){return this.e}}
T.aX8.prototype={}
U.aRx.prototype={
A:function(){var x,w,v,u,t,s,r,q,p,o=this,n=null,m=o.a,l=o.ai(),k=document,j=T.F(k,l)
o.q(j,"ad-preview-card")
o.h(j)
x=T.F(k,j)
T.B(x,"aria-level","2")
o.q(x,"ad-preview-title")
T.B(x,"role","heading")
o.h(x)
x.appendChild(o.e.b)
w=Z.ang(o,3)
o.r=w
v=w.c
j.appendChild(v)
o.ab(v,"ad-preview")
o.h(v)
w=K.Jg()
o.x=w
w=new X.n4(w)
o.y=w
o.r.a4(0,w)
w=T.F(k,j)
o.go=w
T.B(w,"buttonDecorator","")
o.q(o.go,"ad-preview-cta")
o.h(o.go)
o.z=new R.cO(T.cT(o.go,n,!1,!0))
w=M.oc(o,5)
o.Q=w
u=w.c
o.go.appendChild(u)
o.ab(u,"ad-preview-cta-icon")
T.B(u,"icon","search")
o.h(u)
w=new L.iU(u)
o.ch=w
o.Q.a4(0,w)
t=T.aH(k,o.go)
o.q(t,"ad-preview-cta-text")
o.a9(t)
t.appendChild(o.f.b)
w=G.NX(o,8)
o.cx=w
s=w.c
l.appendChild(s)
o.h(s)
w=new V.t(8,n,o,s)
o.cy=w
r=y.y
w=new G.lV(w,new G.br(o,8,C.u),P.bZ(n,n,n,n,!1,r))
o.db=w
o.cx.ad(w,H.a([C.e],y.k))
w=o.go
q=y.h;(w&&C.o).ap(w,"click",o.Z(o.z.a.gbW(),q,y.f))
w=o.go;(w&&C.o).ap(w,"keypress",o.Z(o.z.a.gbL(),q,y.E))
q=o.z.a.b
p=new P.q(q,H.y(q).i("q<1>")).U(o.av(m.gaPe(),y.p))
q=o.db.c
o.b7(H.a([p,new P.aZ(q,H.y(q).i("aZ<1>")).U(o.Z(m.gxk(m),r,r))],y.q))},
a5:function(d,e,f){if(d===C.ef&&3===e)return this.x
if(d===C.p&&4<=e&&e<=7)return this.z.a
if(d===C.dL&&8===e)return this.db
return f},
E:function(){var x,w,v,u,t,s,r,q=this,p=null,o=q.a,n=q.d.f===0
if(n)q.y.c=!0
x=o.b
w=q.dx
if(w!=x)q.dx=q.y.b=x
v=o.a
w=q.dy
if(w!=v){w=q.y
w.d=v
w.a.b.J(0,v)
q.dy=v}u=o.c
w=q.fr
if(w!=u){q.y.a.f.J(0,u)
q.fr=u}t=o.d
w=q.fx
if(w!=t){q.y.a.c.J(0,t)
q.fx=t}s=o.e
w=q.fy
if(w!=s){q.y.a.d.J(0,s)
q.fy=s}if(n){q.ch.saH(0,"search")
r=!0}else r=!1
if(r)q.Q.d.saa(1)
if(n)q.db.sci(o.f)
if(n)q.db.ax()
q.cy.G()
w=T.e("Your ad preview",p,p,p,p)
if(w==null)w=""
q.e.V(w)
q.z.bp(q,q.go)
w=T.e("SEE MORE AD LAYOUTS",p,p,p,p)
if(w==null)w=""
q.f.V(w)
q.r.H()
q.Q.H()
q.cx.H()},
I:function(){var x=this
x.cy.F()
x.r.K()
x.Q.K()
x.cx.K()
x.y.a.R()}}
F.bya.prototype={}
var z=a.updateTypes(["~()","~(cX<@>)","P<u2>()","N<@>()"])
T.aDh.prototype={
$0:function(){H.bL("dialog.1")
return A.fcQ()},
$S:z+2};(function installTearOffs(){var x=a._static_0,w=a._instance_0u,v=a._instance_1i
x(T,"dVp","fnf",3)
var u
w(u=T.apz.prototype,"gaPe","aPf",0)
v(u,"gxk","pV",1)})();(function inheritance(){var x=a.mixin,w=a.inheritMany,v=a.inherit
w(P.C,[T.aX8,F.bya])
v(T.apz,T.aX8)
v(T.aDh,H.aP)
v(U.aRx,E.bV)
x(T.aX8,F.bya)})()
H.ac(b.typeUniverse,JSON.parse('{"aRx":{"n":[],"l":[]}}'))
var y={y:H.b("cX<@>"),h:H.b("b9"),z:H.b("u2"),k:H.b("f<C>"),q:H.b("f<by<~>>"),E:H.b("cs"),f:H.b("cc"),p:H.b("bK")};(function staticFields(){$.hdg=[".ad-preview-card._ngcontent-%ID%{box-shadow:0 2px 2px 0 rgba(0,0,0,.14),0 3px 1px -2px rgba(0,0,0,.12),0 1px 5px 0 rgba(0,0,0,.2);border-radius:2px;background-color:#fff;margin:4px}.ad-preview-title._ngcontent-%ID%{font-weight:bold;font-size:14px;padding:12px 16px}.ad-preview._ngcontent-%ID%{background-color:#f9f9f9;padding:12px 16px;border-top:solid #dadce0 1px;border-bottom:solid #dadce0 1px}.ad-preview-cta._ngcontent-%ID%{color:#1a73e8;cursor:pointer;font-size:14px;font-weight:500;display:flex;align-items:center;padding-top:8px;padding-bottom:8px}.ad-preview-cta-text._ngcontent-%ID%{padding-left:3px}.ad-preview-cta-icon._ngcontent-%ID%{flex:none;padding-left:16px;padding-right:16px}"]
$.duY=null
$.h7d=[$.hdg]})()}
$__dart_deferred_initializers__["N1nykBrPQ99dh0s/kUItIpVEF/k="] = $__dart_deferred_initializers__.current
//# sourceMappingURL=main.dart.js_279.part.js.map
