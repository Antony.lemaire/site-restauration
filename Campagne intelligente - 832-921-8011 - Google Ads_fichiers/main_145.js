self.$__dart_deferred_initializers__=self.$__dart_deferred_initializers__||Object.create(null)
$__dart_deferred_initializers__.current=function(a,b,c,$){var C,H,J,P,W,G={
aeZ:function(d,e){var x,w=new G.aUP(E.ad(d,e,1)),v=$.dzS
if(v==null)v=$.dzS=O.al($.haI,null)
w.b=v
x=document.createElement("material-chips")
w.c=x
return w},
aUP:function aUP(d){var _=this
_.c=_.b=_.a=null
_.d=d}},M,B={pV:function pV(){}},S,Q,K,O,N,X,R,A,L,Y,Z={
U4:function(d,e,f){var x,w=new Z.aw6(N.I(),E.ad(d,e,1),f.i("aw6<0>")),v=$.dze
if(v==null)v=$.dze=O.al($.hab,null)
w.b=v
x=document.createElement("material-chip")
w.c=x
w.ab(x,"themeable")
return w},
aw6:function aw6(d,e,f){var _=this
_.e=d
_.c=_.b=_.a=_.ch=_.Q=_.z=_.y=_.x=_.r=_.f=null
_.d=e
_.$ti=f},
ctH:function ctH(d){this.a=d},
ctI:function ctI(d){this.a=d},
aA7:function aA7(d,e){this.a=d
this.$ti=e},
aA8:function aA8(d,e){var _=this
_.e=_.d=_.c=_.b=null
_.a=d
_.$ti=e}},V={
RT:function(d,e){if(d!=null)return d
return e!=null?"listitem":""},
iq:function iq(d,e,f,g,h,i,j,k){var _=this
_.r="-1"
_.x=d
_.y=null
_.z=!0
_.Q=!1
_.ch=e
_.cy=_.cx=null
_.db=f
_.dx=null
_.b=g
_.c=h
_.d="0"
_.e=i
_.a=j
_.$ti=k}},U={Q6:function Q6(d,e,f,g,h,i,j,k){var _=this
_.a=d
_.b=e
_.c=f
_.d=g
_.e=h
_.f=i
_.r=j
_.x=k
_.y=null
_.z=!1
_.Q=null}},T,F,E={aEy:function aEy(){}},D
a.setFunctionNamesIfNecessary([G,B,Z,V,U,E])
C=c[0]
H=c[1]
J=c[2]
P=c[3]
W=c[4]
G=a.updateHolder(c[5],G)
M=c[6]
B=a.updateHolder(c[7],B)
S=c[8]
Q=c[9]
K=c[10]
O=c[11]
N=c[12]
X=c[13]
R=c[14]
A=c[15]
L=c[16]
Y=c[17]
Z=a.updateHolder(c[18],Z)
V=a.updateHolder(c[19],V)
U=a.updateHolder(c[20],U)
T=c[21]
F=c[22]
E=a.updateHolder(c[23],E)
D=c[24]
E.aEy.prototype={}
U.Q6.prototype={}
V.iq.prototype={
gjy:function(){return this.ch},
gaf:function(d){return this.cx},
zp:function(){var x=this,w=x.cx
if(w==null)x.cy=null
else if(x.ch!==G.Be())x.cy=x.o0(w)},
gcq:function(d){return this.cy},
goi:function(d){var x=this.db
return new P.aZ(x,H.y(x).i("aZ<1>"))},
aST:function(d){var x=this,w=x.y
if(w!=null)w.fM(x.cx)
x.db.J(0,x.cx)
w=J.aE(d)
w.q_(d)
w.tV(d)},
ga3y:function(d){var x=this.dx
return x==null?this.dx=$.eYI().ez():x},
o0:function(d){return this.gjy().$1(d)},
hD:function(d){return this.goi(this).$0()},
sj6:function(d,e){return this.r=e}}
Z.aw6.prototype={
A:function(){var x=this,w=x.a,v=x.ai(),u=x.f=new V.t(0,null,x,T.J(v))
x.r=new K.K(new D.D(u,new Z.ctH(x)),u)
u=T.F(document,v)
x.ch=u
x.q(u,"content")
x.h(x.ch)
x.ch.appendChild(x.e.b)
T.o(x.ch," ")
x.c8(x.ch,1)
u=x.x=new V.t(4,null,x,T.J(v))
x.y=new K.K(new D.D(u,new Z.ctI(x)),u)
J.bE(v,"keydown",x.Z(w.geQ(),y.h,y.E))},
E:function(){var x,w,v=this,u=v.a
v.r.sa1(u.Q)
v.y.sa1(u.z)
v.f.G()
v.x.G()
x=u.ga3y(u)
w=v.z
if(w!=x){v.ch.id=x
v.z=x}w=u.cy
if(w==null)w=""
v.e.V(w)},
I:function(){this.f.F()
this.x.F()},
aj:function(d){var x,w,v=this,u=v.a
if(d){x=u.c
if(x!=null)T.aj(v.c,"role",x)}w=u.r
x=v.Q
if(x!==w){T.aj(v.c,"tabindex",w)
v.Q=w}}}
Z.aA7.prototype={
A:function(){var x=this,w=document.createElement("div")
x.q(w,"left-icon")
x.h(w)
x.c8(w,0)
x.P(w)}}
Z.aA8.prototype={
A:function(){var x,w,v,u,t=this,s="http://www.w3.org/2000/svg",r=document,q=r.createElement("div")
t.e=q
T.B(q,"buttonDecorator","")
t.q(t.e,"delete-button")
t.h(t.e)
t.b=new R.cO(T.cT(t.e,null,!1,!0))
x=C.eE.w5(r,s,"svg")
t.e.appendChild(x)
t.ab(x,"delete-icon")
T.B(x,"height","24")
T.B(x,"viewBox","0 0 24 24")
T.B(x,"width","24")
T.B(x,"xmlns",s)
t.a9(x)
w=C.eE.w5(r,s,"path")
x.appendChild(w)
T.B(w,"d","M12 2c-5.53 0-10 4.47-10 10s4.47 10 10 10 10-4.47 10-10-4.47-10-10-10zm5\n                 13.59l-1.41 1.41-3.59-3.59-3.59 3.59-1.41-1.41 3.59-3.59-3.59-3.59 1.41-1.41 3.59\n                 3.59 3.59-3.59 1.41 1.41-3.59 3.59 3.59 3.59z")
t.a9(w)
q=t.e
v=y.h;(q&&C.o).ap(q,"click",t.Z(t.b.a.gbW(),v,y.f))
q=t.e;(q&&C.o).ap(q,"keypress",t.Z(t.b.a.gbL(),v,y.E))
v=t.b.a.b
q=y.p
u=new P.q(v,H.y(v).i("q<1>")).U(t.Z(t.a.a.gaSS(),q,q))
t.as(H.a([t.e],y.k),H.a([u],y.q))},
a5:function(d,e,f){if(d===C.p&&e<=2)return this.b.a
return f},
E:function(){var x,w=this,v=w.a.a,u=v.x,t=w.c
if(t!=u){T.aj(w.e,"aria-label",u)
w.c=u}x=v.ga3y(v)
t=w.d
if(t!=x){T.aj(w.e,"aria-describedby",x)
w.d=x}w.b.bp(w,w.e)}}
B.pV.prototype={}
G.aUP.prototype={
A:function(){var x=this,w=x.ai(),v=T.F(document,w)
x.q(v,"chipsRow")
x.h(v)
x.c8(v,0)}}
var z=a.updateTypes(["~(@)"])
Z.ctH.prototype={
$2:function(d,e){var x=this.a.$ti
return new Z.aA7(E.E(d,e,x.i("iq<1>")),x.i("aA7<1>"))},
$C:"$2",
$R:2,
$S:2}
Z.ctI.prototype={
$2:function(d,e){var x=this.a.$ti
return new Z.aA8(E.E(d,e,x.i("iq<1>")),x.i("aA8<1>"))},
$C:"$2",
$R:2,
$S:2};(function installTearOffs(){var x=a._instance_1u
x(V.iq.prototype,"gaSS","aST",0)})();(function inheritance(){var x=a.inherit,w=a.inheritMany
x(E.aEy,P.C)
x(U.Q6,N.arD)
x(V.iq,M.ajG)
w(E.bV,[Z.aw6,G.aUP])
w(H.aP,[Z.ctH,Z.ctI])
w(E.v,[Z.aA7,Z.aA8])
x(B.pV,E.aEy)})()
H.ac(b.typeUniverse,JSON.parse('{"iq":{"dB":[],"a6":[],"cN":[]},"aw6":{"n":[],"l":[]},"aA7":{"v":["iq<1>"],"n":[],"u":[],"l":[]},"aA8":{"v":["iq<1>"],"n":[],"u":[],"l":[]},"aUP":{"n":[],"l":[]}}'))
var y={h:H.b("b9"),k:H.b("f<C>"),q:H.b("f<by<~>>"),E:H.b("cs"),f:H.b("cc"),p:H.b("bK")};(function constants(){C.pG=H.w("Q6")})();(function staticFields(){$.hcN=["._nghost-%ID%{background-color:#e0e0e0;color:black;display:flex;align-items:center;border-radius:16px;height:32px;margin:4px;overflow:hidden}.content._ngcontent-%ID%{margin:0 12px;white-space:nowrap;overflow:hidden;text-overflow:ellipsis}.left-icon._ngcontent-%ID%{color:#9e9e9e;fill:#9e9e9e;display:flex;align-items:center;justify-content:center;margin-right:-8px;margin-left:4px;padding:3px}.delete-button._ngcontent-%ID%{border:0;cursor:pointer;outline:none}.delete-button:focus._ngcontent-%ID% .delete-icon._ngcontent-%ID%{fill:#fff}.delete-icon._ngcontent-%ID%{display:flex;background-size:19px 19px;border:0;cursor:pointer;height:19px;margin-left:-8px;margin-right:4px;min-width:19px;padding:3px;width:19px;fill:#9e9e9e}._nghost-%ID%[emphasis]{background-color:#4285f4;color:#fff}._nghost-%ID%[emphasis] .left-icon{color:#fff;fill:#fff}._nghost-%ID%[emphasis] .delete-icon{fill:#fff}._nghost-%ID%[emphasis] .delete-button:focus .delete-icon-svg{fill:#e0e0e0}"]
$.dze=null
$.eg0=[".chipsRow._ngcontent-%ID%{position:relative;display:flex;justify-content:flex-start;flex-flow:row wrap;align-items:center;align-content:space-around;width:100%;height:100%;vertical-align:top;margin:0;padding:0}._nghost-%ID%{margin:0;padding:0;display:block;vertical-align:top}material-chip:last-of-type._ngcontent-%ID%{margin-right:16px}"]
$.dzS=null
$.hab=[$.hcN]
$.haI=[$.eg0]})();(function lazyInitializers(){var x=a.lazy
x($,"i2V","OP",function(){return T.e("Delete",null,"chipDeleteButtonMessage",null,"Label for a button which removes the item when clicked.")})
x($,"ikA","eYI",function(){return R.cVx()})})()}
$__dart_deferred_initializers__["DpIDJvw9QNi1bqlWnVtaZnDOl/c="] = $__dart_deferred_initializers__.current
//# sourceMappingURL=main.dart.js_88.part.js.map
