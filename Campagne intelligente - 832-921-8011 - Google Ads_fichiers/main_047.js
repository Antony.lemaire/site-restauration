self.$__dart_deferred_initializers__=self.$__dart_deferred_initializers__||Object.create(null)
$__dart_deferred_initializers__.current=function(a,b,c,$){var C,H,J,P,W,G,M,B={
cNB:function(d){return C.c.Kc(C.n.b4(d*1e7),-2147483648,2147483647)}},S,Q={
hiv:function(d){return d.a.a7(14)?d.a.a3(2):null},
d_5:function(d){return J.c2(d,new Q.cGv()).C(0,Q.hiK(),y.c)},
cGv:function cGv(){}},K,O,N,X,R,A,L,Y,Z,V,U,T,F={
c0_:function(d){var x,w,v,u,t,s,r,q,p,o,n
d.T(2,C.a.ar(C.bAo,d.a.O(1))?d.a.O(1):C.cM)
x=y.b
x=new F.DV(null,d,P.hV(y.g),C.eD,new D.wN(P.Y(x,x)),N.bJ("GeoTarget"))
w=Z.atz()
w.be(0,d.a.a3(6).aX(0))
w.be(1,d.a.a3(7).aX(0))
x.Q=w
v=d.a.O(2)
v=d.a.O(1)===C.cM?v*1609.344:v*1000
u=w.a.b2(0)/1e7
t=w.a.b2(1)/1e7
s=v/110574
r=u-s
w=Math.cos(r/180*3.141592653589793)
q=u+s
p=Math.cos(q/180*3.141592653589793)
o=Z.cVl()
n=Z.atz()
n.be(0,B.cNB(r))
n.be(1,B.cNB(t-v/(111320*w)))
o.T(1,n)
n=Z.atz()
n.be(0,B.cNB(q))
n.be(1,B.cNB(t+v/(111320*p)))
o.T(2,n)
x.z=o
return x},
c12:function c12(){},
aPd:function aPd(){},
ci2:function ci2(d){this.a=d},
DV:function DV(d,e,f,g,h,i){var _=this
_.x=d
_.y=e
_.Q=_.z=null
_.d=f
_.a=g
_.b=h
_.c=i},
b8x:function b8x(){}},E,D
a.setFunctionNamesIfNecessary([B,Q,F])
C=c[0]
H=c[1]
J=c[2]
P=c[3]
W=c[4]
G=c[5]
M=c[6]
B=a.updateHolder(c[7],B)
S=c[8]
Q=a.updateHolder(c[9],Q)
K=c[10]
O=c[11]
N=c[12]
X=c[13]
R=c[14]
A=c[15]
L=c[16]
Y=c[17]
Z=c[18]
V=c[19]
U=c[20]
T=c[21]
F=a.updateHolder(c[22],F)
E=c[23]
D=c[24]
F.c12.prototype={}
F.aPd.prototype={
ga2x:function(){return"\u2014"},
gC7:function(){return C.uv},
az8:function(d,e,f){var x=null
switch(d){case C.cM:return T.e(e+" mi around "+f,x,"RadiusGeoTarget__milesRadiusTargetDescription",H.a([e,f],y.h),x)
case C.fi:return T.e(e+" km around "+f,x,"RadiusGeoTarget__kilometersRadiusTargetDescription",H.a([e,f],y.h),x)
default:this.c.b0(C.ae,new F.ci2(d),x,x)
return}}}
F.DV.prototype={
glE:function(d){return this.z},
gZn:function(){return this.Q},
gbY:function(d){var x=this.y
return this.az8(x.a.O(1),this.b.e7(x.a.O(2),"#.#"),x.a.Y(12))},
gpi:function(){return this.y.a.Y(12)},
uL:function(d){var x,w,v
if(d instanceof F.DV)if(this.Q.am(0,d.Q)){x=this.y
w=x.a.O(2)
v=d.y
x=w==v.a.O(2)&&x.a.O(1)==v.a.O(1)}else x=!1
else x=!1
return x},
gKH:function(){var x=this.Q,w=this.y,v=w.a.O(2)
w=w.a.O(1)===C.cM?v*1609.344:v*1000
return X.xF(this.x,x,w)},
am:function(d,e){if(e==null)return!1
return this.a8A(0,e)},
ghf:function(){return this.x}}
F.b8x.prototype={}
var z=a.updateTypes(["O(as)","L<O>(L<as>)"])
Q.cGv.prototype={
$1:function(d){return d.a.a7(14)},
$S:27}
F.ci2.prototype={
$0:function(){return"No translation for distance unit: "+H.p(this.a)},
$C:"$0",
$R:0,
$S:3};(function installTearOffs(){var x=a._static_1
x(Q,"hiK","hiv",0)
x(Q,"hiI","d_5",1)})();(function inheritance(){var x=a.mixin,w=a.inheritMany,v=a.inherit
w(H.aP,[Q.cGv,F.ci2])
v(F.c12,P.C)
v(F.aPd,F.bj)
v(F.b8x,F.aPd)
v(F.DV,F.b8x)
x(F.b8x,F.c12)})()
H.ac(b.typeUniverse,JSON.parse('{"aPd":{"bj":[]},"DV":{"bj":[]}}'))
var y={c:H.b("O"),h:H.b("f<C>"),g:H.b("c"),b:H.b("@")};(function constants(){var x=a.makeConstList
C.bAo=H.a(x([C.cM,C.fi]),H.b("f<mJ>"))})()}
$__dart_deferred_initializers__["D6m2rMwSDOJItPcM7nzmMqFnp/g="] = $__dart_deferred_initializers__.current
//# sourceMappingURL=main.dart.js_87.part.js.map
